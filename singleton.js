var Demo = (function() {
	
	
	var TAG = 'Demo';
		
	/**
	* Public service methods
    */
	var interface = {
		
		showModal : function(){
				console.log('showModal');
		},
		
		init : function() {
			console.log('initializing...');
			presenter.init();
		}
	};
	
	
		
	var viewModel = {
			
			watchExampleVM : new Vue({
				  el: '#watch-example',
				  
				  data: {
				    question: '',
				    answer: 'I cannot give you an answer until you ask a question!',
				    test: ''
				  },
				  
				  watch: {
				    // whenever question changes, this function will run
				    question: function (newQuestion, oldQuestion) {
				    	console.log('watch calls question');
				      this.answer = 'Waiting for you to stop typing...'
				      this.debouncedGetAnswer()
				    },
				    
				    test : function(newcontent, oldcontent,next){
				    	console.log('test has changed');
				    	console.dir(newcontent +'  |  ' + oldcontent);
				    }
				  },
				  
				  created: function () {
					console.log('created is called');
				    this.debouncedGetAnswer = _.debounce(this.getAnswer, 500)
				  },
				  
				  methods: {
				    getAnswer:  function () {
				    	console.log('getAnswer is called');
				      if (this.question.indexOf('?') === -1) {
				        this.answer = 'Questions usually contain a question mark. ;-)'
				        return
				      }
				      this.answer = 'Thinking...'
				      var vm = this
				      axios.get('https://yesno.wtf/api')
				        .then(function (response) {
				          vm.answer = _.capitalize(response.data.answer)
				        })
				        .catch(function (error) {
				          vm.answer = 'Error! Could not reach the API. ' + error
				        })
				    }
				  }
				}),
				
				
				clickCounter: new Vue({
					  el: '#example-1',
					  data: {
					    counter: 0
					  },
					  
					  setValue : function(val){
						  console.log(val);
					  },
					})

	};
	
	var View = function(model){
		
		this.model = model;
		
		// reference inside closures
		var self = this;
		
		this.init = function() {
			console.dir(this.model);
			
		};

		// Populate the view model
		this.setValues = function(data) {
			this.model.clickCounter.setValue(10);
		};

		// apply the values to the persisted entity
		this.applyValues = function() {

		};
		
		this.reset = function(){
			
		}
		
	};
	
	var presenter = {
			
			callback : function(){},

			init : function() {
				view = new View(viewModel);
				view.init();
			},
			
			setCallback : function(callback){
				this.callback = callback;
			},

			setInitData : function(data) {
				view.setValues(data);
			},

			resetView : function() {
				view.reset();
			}
		};
	
	
	return interface;

})();